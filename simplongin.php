<?php

/**
 * Plugin Name: Simplon Plugin
 * Description: Un plugin fait en formation pour l'apprentissage
 * Version: 0.0.1
 * Author: Simplon
 */

add_action("admin_menu", "simplon_menu");

function simplon_menu()
{
    add_menu_page(
        "Simplon Admin",
        "Simplon",
        "manage_options",
        "simplon_admin_menu",
        "simplon_admin_page"
    );

    add_submenu_page(
        "simplon_admin_menu",
        "Simplon Configuration",
        "Configuration",
        "manage_options",
        "simplon_admin_menu_configuration",
        "simplon_admin_page"
    );
    /**
     * On peut rajouter ça pour supprimer le sous menu dupliqué
     */
    remove_submenu_page("simplon_admin_menu", "simplon_admin_menu");

    add_action("admin_init", "simplon_settings");
}

function simplon_settings()
{
    register_setting("simplon_group", "admin_color", [
        "default" => "#fff"
    ]);
    register_setting("simplon_group", "admin_title_size", [
        "default" => "2"
    ]);
}

function simplon_admin_page()
{
    require 'admin-menu.php';
}

add_action("admin_head", "simplon_admin_style");

function simplon_admin_style()
{
?>
    <style>
        #wpadminbar,
        #adminmenu {
            background-color: <?= get_option("admin_color") ?>;
        }

        .wp-admin h1 {
            font-size: <?= get_option("admin_title_size") ?>em;
        }
    </style>
<?php
}

add_action("init", "simplon_post_type");

function simplon_post_type()
{


    register_post_type("promo", [
        "label" => "Promos",
        "public" => true,
        "supports" => ["title"]
    ]);
}

add_action("add_meta_boxes", "promo_fields");

function promo_fields()
{
    add_meta_box("promo_referentiel", "Champs promo", "display_referentiel", "promo");
}

function display_referentiel()
{
    $id = get_the_ID();
    
?>
<label for="referentiel">Référentiel</label>
<input type="text" name="referentiel" id="referentiel" 
value="<?= get_post_meta($id, "referentiel", true) ?>">

<?php
}

add_action("save_post", "promo_form", 10, 2);

function promo_form(int $id, WP_Post $post) {
    if($post->post_type != "promo") {
        return;
    }
    update_post_meta($id, "referentiel", $_POST["referentiel"]);
}